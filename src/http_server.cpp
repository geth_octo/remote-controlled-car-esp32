#include "http_server.hh"

httpd_handle_t server = NULL; 

void parseUri(const char * received_uri);

esp_err_t setUdpCliInfoHandler(httpd_req_t *req)
{
    httpd_resp_set_hdr(req, "Access-Control-Allow-Origin", "*");

    /* Send a simple response */
    Serial.println("New GET req");
    Serial.println(req->uri);
    
    parseUri(req->uri);

    const char resp[] = "Starting UDP connection";
    httpd_resp_send(req, resp, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

/* URI handler structure for GET /start */
httpd_uri_t setUdpCliInfo = {
    .uri      = "/start",
    .method   = HTTP_GET,
    .handler  = setUdpCliInfoHandler,
    .user_ctx = NULL
};

/* Function for starting the webserver */
void start_server(void)
{
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    /* Start the httpd server */
    if (httpd_start(&server, &config) == ESP_OK) {
        /* Register URI handlers */
        httpd_register_uri_handler(server, &setUdpCliInfo);
    }
}

void parseUri(const char * received_uri) {
    char *uri_params, *param;
    char *port = NULL, *ip = NULL;
    char *uri = (char*) malloc(sizeof(char) * (strlen(received_uri) + 1));

    strcpy(uri, received_uri);
    strtok (uri,"?");
    uri_params = strtok (NULL, "?");

    Serial.println(uri_params);
    param = strtok (uri_params, "&=");

    while (param) {
        Serial.println(param);
        if(!strcmp(param, "ip")) {
            ip = strtok (NULL, "&=");
        }
        if(!strcmp(param, "port")) {
            port = strtok (NULL, "&=");
        }
        param = strtok (NULL, "&=");
    }

    if(port && ip) {
        int ipAddr[4];

        ipAddr[0] = atoi(strtok (ip, "."));
        for(int i = 1; i < 4; i++) {
            ipAddr[i] = atoi(strtok (NULL, "."));
        }

        WRITING_PORT = atoi(port);
        CLI_IP_ADDR = IPAddress(ipAddr[0], ipAddr[1], ipAddr[2], ipAddr[3]);

        Serial.print("Cli IP : ");
        Serial.println(CLI_IP_ADDR);
        Serial.printf("Writing port : %d\n", WRITING_PORT);
    }
}