#include "WiFi.h"
#include "AsyncUDP.h"
#include "main.hh"

#ifdef UDP_CONNECTION
AsyncUDP udp;
#endif

double last_speed, last_direction;
camera_fb_t* frameBuffer = NULL;

 
void setup()
{
  Serial.begin(115200);

  #ifdef MQTT_CONNECTION
  initWiFi();
  mqtt_app_start();
  #endif

  #ifdef UDP_CONNECTION
  initWiFi();
  start_server();
  initConnection();
  #endif

  initLed();
  initEngines();
  initCamera();

  #ifdef BLUETOOTH_CONNECTION
  initBluetoothServer();
  #endif 
}


void loop()
{
  takePicture();
  frameBuffer = getFrameBuffer();

  #ifdef UDP_CONNECTION
  sendUdpData(frameBuffer->buf, frameBuffer->len);
  #elif defined BLUETOOTH_CONNECTION
  sendBluetoothImage(frameBuffer->buf, frameBuffer->len);
  #endif

  #ifdef MQTT_CONNECTION
  publish_msg(frameBuffer->buf, frameBuffer->len);
  #endif

  freeFrameBuffer();  

  #ifdef UDP_CONNECTION
  delay(100);
  #elif defined BLUETOOTH_CONNECTION
  delay(7);
  #endif
}
