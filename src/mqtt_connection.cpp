#include "mqtt_connection.hh"

esp_mqtt_client_handle_t client;

void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    #ifdef DEBUG
    Serial.printf("Event dispatched from event loop base=%s, event_id=%d\n", base, event_id);
    #endif

    esp_mqtt_event_handle_t event = (esp_mqtt_event_handle_t) event_data;
    esp_mqtt_client_handle_t client = event->client;
    switch ((esp_mqtt_event_id_t)event_id) {
    case MQTT_EVENT_CONNECTED:
        #ifdef DEBUG
        Serial.println("MQTT_EVENT_CONNECTED");
        #endif

        esp_mqtt_client_subscribe(client, CONTROL_TOPIC, 0);

    case MQTT_EVENT_DISCONNECTED:
        #ifdef DEBUG
        Serial.println("MQTT_EVENT_DISCONNECTED");
        #endif
        break;
    case MQTT_EVENT_SUBSCRIBED:
        break;
    case MQTT_EVENT_UNSUBSCRIBED:
        break;
    case MQTT_EVENT_PUBLISHED:
        break;
    case MQTT_EVENT_DATA:
        decodeJsonPayload((char *) event->data);
        break;
    case MQTT_EVENT_ERROR:
        #ifdef DEBUG
        Serial.println("MQTT_EVENT_ERROR");
        #endif
        break;
    default:
        #ifdef DEBUG
        Serial.printf("Other event id:%d\n", event->event_id);
        #endif
        break;
    }
}


void mqtt_app_start()
{
    esp_mqtt_client_config_t mqtt_cfg = {};
    mqtt_cfg.host = BROKER_HOST;
    mqtt_cfg.port = BROKER_PORT;
    mqtt_cfg.username = BROKER_USERNAME;
    mqtt_cfg.password = BROKER_PASSWORD;
    mqtt_cfg.buffer_size = BUFFER_SIZE;
;


    client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_register_event(client, (esp_mqtt_event_id_t) ESP_EVENT_ANY_ID, mqtt_event_handler, NULL);
    esp_mqtt_client_start(client);
}

int publish_msg(uint8_t * buffer, size_t len, const char * topic, int qos, int retain) 
{
    return esp_mqtt_client_publish(client, topic, (char *) buffer, len, qos, retain);
}