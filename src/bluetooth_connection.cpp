#include "bluetooth_connection.hh"

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <BLE2902.h>

BLEServer *pServer;
BLEService *pService;
BLECharacteristic *pCamCharacteristic;
BLECharacteristic *pCtrlCharacteristic;
BLE2902 *p2902Descriptor;

class CtrlCharacteristicCallbacks : public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
        #ifdef DEBUG
        Serial.println((char*) pCharacteristic->getData());
        #endif

        decodeJsonPayload((char*) pCharacteristic->getData());
    }
};

void initBluetoothServer() {
    BLEDevice::init(SERVER_NAME);
    pServer = BLEDevice::createServer();
    pService = pServer->createService(SERVICE_UUID);
    pCamCharacteristic = pService->createCharacteristic(
                                            CAM_CHARACTERISTIC_UUID,
                                            BLECharacteristic::PROPERTY_READ    |
                                            BLECharacteristic::PROPERTY_NOTIFY
                                        );

    pCtrlCharacteristic = pService->createCharacteristic(
                                            CTRL_CHARACTERISTIC_UUID,
                                            BLECharacteristic::PROPERTY_WRITE    |
                                            BLECharacteristic::PROPERTY_NOTIFY
                                        );

    p2902Descriptor = new BLE2902();
    pCamCharacteristic->addDescriptor(p2902Descriptor);
    pCtrlCharacteristic->setCallbacks(new CtrlCharacteristicCallbacks());
    pService->start();
    BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
    pAdvertising->addServiceUUID(SERVICE_UUID);
    pAdvertising->setScanResponse(true);
    pAdvertising->setMinPreferred(0x06);  // functions that help with iPhone connections issue
    pAdvertising->setMinPreferred(0x12);
    BLEDevice::startAdvertising();
}

void modifyCameraServiceValue(uint8_t *buffer, size_t len) {
    pCamCharacteristic->setValue(buffer, len);
    pCamCharacteristic->notify();
}

void sendBluetoothImage(uint8_t *buffer, size_t len) {
    if(getConnectedCount()) {
        int bytesSent = 0;
        uint8_t buff[500];
        sprintf((char*) buff, "{\"size\": %d}", len);
        modifyCameraServiceValue(buff, strlen((char*) buff));
        while(bytesSent < len){
            size_t packet_size = len - bytesSent >= 500 ? 500 : len - bytesSent;
            memcpy((char*) buff, buffer + bytesSent, packet_size);
            bytesSent += packet_size;
            modifyCameraServiceValue(buff, packet_size);
            #ifdef DEBUG
            Serial.print(bytesSent);
            Serial.print("/");
            Serial.println(len);
            #endif
            delay(7);
        }
    }
}

int getConnectedCount() {
    return pServer->getConnectedCount();
}