#include "udp_connection.hh"

IPAddress CLI_IP_ADDR;
int WRITING_PORT;

void initConnection() {
    if (udp.listen(LISTENING_PORT)) {
    
    udp.onPacket([](AsyncUDPPacket packet) {
      char* data = (char*)packet.data();
      
      decodeJsonPayload(data);
    });
  }
}

void sendUdpData(uint8_t *buff, size_t len) {
    AsyncUDPMessage msg = AsyncUDPMessage(len);
    msg.write(buff, len);

    #ifdef DEBUG
    Serial.println(len);
    #endif

    if( CLI_IP_ADDR && WRITING_PORT) {
      udp.sendTo(msg, CLI_IP_ADDR, WRITING_PORT);

      #ifdef DEBUG
      Serial.println("Sending UDP data");
      #endif
    }
}