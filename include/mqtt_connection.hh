# pragma once

#include <Arduino.h>
#include "config.h"
#include "mqtt_client.h"
#include "json_decoder.hh"

#define CAMERA_STREAM_TOPIC "/camera"
#define CONTROL_TOPIC       "/control"

#define BROKER_HOST "192.168.1.122"
#define BROKER_PORT 1883
#define BROKER_USERNAME "STAGE_OCTO"
#define BROKER_PASSWORD "remotecontrolledcar"

#define BUFFER_SIZE 24800

void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data);
void mqtt_app_start();

int publish_msg(uint8_t * buffer, size_t len, const char * topic = CAMERA_STREAM_TOPIC, int qos = 0, int retain = 0);
