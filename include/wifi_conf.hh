#pragma once

#include "WiFi.h"
#include "esp_wifi.h"
#include "nvs_flash.h"
#include "config.h"

void initWiFi();