#pragma once

#include "wifi_conf.hh"
#include "engines_management.hh"
#include "camera.hh"
#include "udp_connection.hh"
#include "bluetooth_connection.hh"
#include "AsyncUDP.h"
#include "http_server.hh"
#include "led.hh"
#include "config.h"
#include "mqtt_connection.hh"

extern double last_speed, last_direction;

extern AsyncUDP udp;